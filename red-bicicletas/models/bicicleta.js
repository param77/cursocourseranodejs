var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
    return "id: " + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(biciId) {
    var aBici = Bicicleta.allBicis.find(x=> x.id == biciId);
    if (aBici)
        return aBici;
    else
        throw new Error('No existe una bicicleta con el id ${biciId}');
}

Bicicleta.removeByID = function(abiciId) {
    //var aBici = findById(abiciId);
    for(var i=0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == abiciId) {
            Bicicleta.allBicis.splice(i,1);
            break;
        } 

    }

}

var a = new Bicicleta(1, "roja","urbana", [43.3302992, -3.1290623]);
var b = new Bicicleta(2, "blanca","urbana", [43.3312992, -3.1290623]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;