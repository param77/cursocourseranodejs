var map = L.map('main_map').setView([43.3302992, -3.1290623], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//L.marker([43.3, -3.12]).addTo(map);
//L.marker([43.4, -3.12]).addTo(map);
//L.marker([43.5, -3.12]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        });
    }
})